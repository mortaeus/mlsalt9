ó
µ)ŖVc           @   s	  d  Z  d d l Z d d l Z d d l Z e j d  Z d d d     YZ d   Z d   Z d   Z	 e
 d	  Z e
 d
  Z d d  Z e d k rd d l Z d d l Z e j e d   Z d Z e j e e d e i  e e  Z e j e d d GHn  d S(   sL  
dact.py - dialogue act specification
========================================

Author: Dongho Kim  (Copyright CUED Dialogue Systems Group 2015)

**Basic Usage**: 
    >>> import dact
   
.. Note::

    Copied from dstc-ii code

.. seealso:: CUED Imports/Dependencies: 

    import :class:`ContextLogger`

************************

i’’’’Nt    t   DactItemc           B   sD   e  Z d  Z d   Z d   Z d   Z d   Z d   Z d   Z RS(   s~   Dialogue act specification

    :param slot: (str) slot name
    :param op: (str) comparative operation, e.g. '=' or '!='
    c         C   sz   | |  _  | |  _ | |  _ | d  k	 rv t |  t t g k rv t |  d k rv | d t j	 k rv | j
   |  _ n  d  S(   Ni    (   t   slott   opt   valt   Nonet   typet   strt   unicodet   lent   stringt   punctuationt   lower(   t   selfR   R   R   (    (    s>   /home/dial/phs26/MPhil_practical/cued-python_practical/dact.pyt   __init__%   s
    			Ic         C   sF  |  j  | j  k r t S|  j d k s4 | j d k rW t j d t |   t |   n  |  j d k r¢ | j d k r¢ |  j | j k r t S|  j d k r t St S|  j d k rš | j d k rš |  j d k rÓ t S|  j | j k ré t St SnR |  j d k r>| j d k r>| j d k r!t S|  j | j k r7t St Sn t Sd S(   sĶ  
        Commutative operation for comparing two items.
        Note that "self" is the goal constraint, and "other" is from the system action.
        The item in "other" must be more specific. For example, the system action confirm(food=dontcare) doesn't match
        the goal with food=chinese, but confirm(food=chinese) matches the goal food=dontcare.

        If slots are different, return True.

        If slots are the same, (possible values are x, y, dontcare, !x, !y, !dontcare)s
            x, x = True
            x, y = False
            dontcare, x = True
            x, dontcare = False
            dontcare, dontcare = True

            x, !x = False
            x, !y = True
            x, !dontcare = True
            dontcare, !x = False
            dontcare, !dontcare = False

            !x, !x = True
            !x, !y = True
            !x, !dontcare = True
            !dontcare, !dontcare = True

        :param other:
        :return:
        s3   None value is given in comparison between %s and %st   =t   dontcares   !=N(	   R   t   TrueR   R   t   loggert   errorR   R   t   False(   R   t   other(    (    s>   /home/dial/phs26/MPhil_practical/cued-python_practical/dact.pyt   match2   s.    #c         C   s>   |  j  | j  k r: |  j | j k r: |  j | j k r: t St S(   N(   R   R   R   R   R   (   R   R   (    (    s>   /home/dial/phs26/MPhil_practical/cued-python_practical/dact.pyt   __eq__p   s    6c         C   s"   t  t |  j |  j |  j f   S(   N(   t   hasht   reprR   R   R   (   R   (    (    s>   /home/dial/phs26/MPhil_practical/cued-python_practical/dact.pyt   __hash__u   s    c         C   s   t  |  j |  j |  j f  S(   N(   R   R   R   R   (   R   (    (    s>   /home/dial/phs26/MPhil_practical/cued-python_practical/dact.pyt   __str__x   s    c         C   s   t  |  j |  j |  j f  S(   N(   R   R   R   R   (   R   (    (    s>   /home/dial/phs26/MPhil_practical/cued-python_practical/dact.pyt   __repr__{   s    (	   t   __name__t
   __module__t   __doc__R   R   R   R   R   R   (    (    (    s>   /home/dial/phs26/MPhil_practical/cued-python_practical/dact.pyR      s   		>			c         C   sO  i  } g  | d <|  d k r* d | d <| St  j d |  j    } | sV d | d <| S| j d  j   | d <| j d  } xŹt |  d k rJt  j d	 |  } | r+| j d  j   } | j d  j   } | j d
  j d  } t | | |  } t  j d	 d |  } | d j |  q n  t  j d |  } | rĆ| j d  j   } | j d  j   } | j d
  j d  } t | | |  } t  j d d |  } | d j |  q n  t  j d |  } | r:| j d  j   } d  } d  } t | | |  } t  j d d |  } | d j |  q n  t	 d |  q W| S(   Nt   slotss	   BAD ACT!!t   nullt   acts   ^([^\(\)]*)\((.*)\)$i   i   i    s    ^([^,!=]*)(!?=)\s*"([^"]*)"\s*,?i   s   ' R    s   ^([^,=]*)(!?=)\s*([^,]*)\s*,?s
   ^([^,]*),?s   Cant parse content fragment: %s(
   t   ret   searcht   stript   groupR	   R   t   subt   appendR   t   RuntimeError(   t   tt   rt   mt   contentR   R   R   t   items(    (    s>   /home/dial/phs26/MPhil_practical/cued-python_practical/dact.pyt   _InParseAct   sR    


c         C   sL  i  } t    | d <|  d k r- d | d <| St j d |  j    } | sY d | d <| S| j d  | d <| j d  } xŹt |  d k rGt j d	 |  } | r(| j d  j   } | j d  j   } | j d
  j d  } t | | |  } t j d	 d |  } | d j |  q~ n  t j d |  } | rĄ| j d  j   } | j d  j   } | j d
  j d  } t | | |  } t j d d |  } | d j |  q~ n  t j d |  } | r7| j d  j   } d  } d  } t | | |  } t j d d |  } | d j |  q~ n  t
 d |  q~ W| S(   NR    s	   BAD ACT!!R!   R"   s   ^([^\(\)]*)\((.*)\)$i   i   i    s    ^([^,!=]*)(!?=)\s*"([^"]*)"\s*,?i   s   ' R    s   ^([^,=]*)(!?=)\s*([^,]*)\s*,?s
   ^([^,]*),?s   Cant parse content fragment: %s(   t   setR#   R$   R%   R&   R	   R   R'   t   addR   R)   (   R*   R+   R,   R-   R   R   R   R.   (    (    s>   /home/dial/phs26/MPhil_practical/cued-python_practical/dact.pyt   _InParseActSet±   sR    

c         C   sy  i  } g  | d <|  d k r* d | d <| St  j d |  j    } | sV d | d <| S| j d  | d <| j d  } xvt |  d k ršt  j d	 |  } | r| j d  j   } | j d  j d
  } t  j d	 d |  } | d j | | g  q{ n  t  j d |  } | r{| j d  j   } | j d  j d
  } t  j d d |  } | d j | | g  q{ n  t  j d |  } | rą| j d  j   } d  } t  j d d |  } | d j | | g  q{ n  t d |  q{ Wx | d D]u } | d d  k rqün  | d j	   | d <| d d k rüy t
 | d  } | | d <Wqqt k
 rmqqXqüqüW| S(   NR    s	   BAD ACT!!R!   R"   s   ^([^\(\)]*)\((.*)\)$i   i   i    s   ^([^,=]*)=\s*"([^"]*)"\s*,?s   ' R    s   ^([^,=]*)=\s*([^,]*)\s*,?s
   ^([^,]*),?s   Cant parse content fragment: %st   count(   R#   R$   R%   R&   R	   R'   R(   R   R)   R   t   intt
   ValueError(   R*   R+   R,   R-   R   R   t	   slot_pairt	   int_value(    (    s>   /home/dial/phs26/MPhil_practical/cued-python_practical/dact.pyt
   __ParseActć   sZ    


c         C   sÜ  t  |   } g  } | d d k r5 | r5 d | d <n  | d } | d d k s_ | d d k rß x[ g  | d D] \ } } | d  k rm | ^ qm D]* } | j i d d 6d | g g d 6 q W| d d k rÖ d } qid } n | d d0 k r;| d d k r| rd | d <n  | j i | d d 6g  d 6 d } n. | d d1 k ri|  GH| GHt d | d  n  | d d k r| rd } n  | d d k rŅ| rŅd g  | d D] \ } }	 |	 ^ q®k rŅd } n  | d d k rt | d  d k r| j i d	 d 6g  d 6 n  d d  g | d k rķ| rķg  }
 g  } xh | d D]\ \ } } | d  k sJ| d k rtqJn  | d! k r| j |  qJn  |
 j | | g  qJWi d" d 6|
 d 6g g  | D]# } i d# d 6d | g g d 6^ qÅS| rMd  g  | d D] \ } }	 |	 ^ qk rM| d d k rBi d
 d 6g  d 6g Sg  | d D] \ } }	 |	 d  k rM| ^ qM} g  | d D] \ } }	 | d k r||	 ^ q|\ } g  | d D]$ \ } } | d  k r®| | g ^ q®}
 | j i d$ d 6g  | D] } d | g ^ qļd | g g d 6 |
 r6i d d 6|
 d 6} qMi g  d 6d d 6} n  d g  | d D] \ } } | ^ q[k r| rg  | d D] \ } } | d k r| ^ q\ } g  | d D]$ \ } } | d k rŗ| | g ^ qŗ}
 | j i d% d 6d | g g d 6 |
 | d <n  i  } x | d D] \ } } | } | } | } | | k r]i  | | <n  | | | k r~g  | | | <n  | | | | k r&| | | j |  q&q&Wxš| j   D]ā\ } } xÓ| j   D]Å\ } } | d& k rõd' } n  | d k r~t |  d( k r~d) | k r~| d } | d* } | j i d d 6| | g g d 6 | j i d d 6| | g g d 6 qŌx| D]} | d  k s| d k r©qn  t |  d k r| d+ d, k r| d+  } | | g g } | j i d d 6| d 6 q| d- k r5| j i d. d 6| | g g d 6 q| | g g } | | f d2 k rh| d k rhqn  | j i | d/ k rd n | d 6| d 6 qWqŌWq»W| rŲt |  d k rŲ| j i d
 d 6g  d 6 n  | S(3   NR"   t   selectt   informt   requestt   confreqR    R   s	   impl-conft   negatet   repeatt   affirmt   byet   restartt   reqaltst   hellot   silencet   thankyout   ackt   helpt   cantheart   reqmoret
   welcomemsgt   denyt   confirmR!   t   badacts(   Dont know how to convert raw act type %ss	   expl-confR   i    t   namet   nones   name!t   canthelps   canthelp.exceptions   canthelp.missing_slot_valuet   offerR    t   thisi   R   i   i’’’’t   !t   optiont
   giveoptionR3   (   s   negates   repeats   affirms   byes   restarts   reqaltss   helloRD   s   thankyous   acks   helpRH   s   reqmore(   s   informs   denys   confirms   selects   nulls   badact(   s   thiss   dontcare(   R8   R   R(   R)   R	   R.   (   t   raw_act_textt   usert   raw_actt   final_dialog_actt   main_act_typeR   t   valuet   requested_slott   st   vt   other_slotst
   only_namesRN   t   _t
   none_slotst
   name_valuet	   none_slott   main_act_slots_dictt   raw_slot_namet   raw_slot_valt	   slot_namet   slot_valt
   slot_groupt   slot_group_namet   slot_group_itemst   valst   false_valuet
   true_valueR   R    (    (    s>   /home/dial/phs26/MPhil_practical/cued-python_practical/dact.pyt	   _ParseAct  sŽ    
 6						@	&	
/0/25	1025		*

		"
			c         C   s:   g  } x- |  j  d  D] } | t | d | 7} q W| S(   Nt   |RW   (   t   splitRp   (   RV   RW   t   finalt   act_text(    (    s>   /home/dial/phs26/MPhil_practical/cued-python_practical/dact.pyt   ParseActĮ  s    c   	      C   sC  x<|  D]4} x+t  d t | d   D]} | d | \ } } | d k r| d k rt } | rĢ xj | d j   D]O \ } } | | k rv | d k r¼ d | d | d | GHt } n  | } qv qv Wn d	 } | s| d k rž d
 | GHd | d | <q| | f | d | <qn  t d   | d D  | d (q' Wq W|  S(   Ni    R    RR   R   t
   informables	   Warning: s    could be for s    or R   s'   Warning: unable to find slot for value R    c         s   s9   |  ]/ \ } } | d  k s' | d  k r | | f Vq d S(   R    N(    (   t   .0R]   R^   (    (    s>   /home/dial/phs26/MPhil_practical/cued-python_practical/dact.pys	   <genexpr>Ž  s    (   R    R    (   t   rangeR	   R   t	   iteritemsR   t   list(	   t   uactst   ontologyt   uactt   indexR   R[   t   skipThisR]   Rm   (    (    s>   /home/dial/phs26/MPhil_practical/cued-python_practical/dact.pyt   inferSlotsForActČ  s(     		't   __main__s>   /home/dial/mh521/DSTC/GM/scripts/config/ontology_dstc2_da.jsons-   inform(name="yu garden",phone="01223 248882")RW   t   indenti   (    (   R   R#   R
   t   ContextLoggert	   getLoggerR   R   R/   R2   R8   R   Rp   Ru   R   R   R   t   jsont	   boostutilt   loadt   openR|   R"   t   transformActR   t   testt   dumps(    (    (    s>   /home/dial/phs26/MPhil_practical/cued-python_practical/dact.pyt   <module>   s"   `	2	2	5©$